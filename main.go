package main

import (
	"archive/zip"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"layeh.com/gumble/gumble"
	"layeh.com/gumble/gumbleutil"
	_ "layeh.com/gumble/opus"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalln(err)
	}
	checkEnv()

	config := gumble.NewConfig()
	config.Username = os.Getenv("USERNAME")

	config.Attach(gumbleutil.Listener{
		ChannelChange: handleChannelChange,
	})
	client, err := gumble.DialWithDialer(new(net.Dialer), fmt.Sprintf("%s:%s", os.Getenv("REMOTE_URL"), os.Getenv("REMOTE_PORT")), config, &tls.Config{InsecureSkipVerify: true})
	if err != nil {
		log.Fatalln(err)
	}

	startServer(client)
}

func handleChannelChange(e *gumble.ChannelChangeEvent) {
	urlString := os.Getenv("LOCAL_URL") + ":" + os.Getenv("LOCAL_PORT")
	e.Channel.Send(fmt.Sprintf("<br><a href=\"%s\">UPLOAD FILE</a><br><a href=\"%s/files\">BROWSE FILES</a>", urlString, urlString), false)
}

func checkEnv() {
	if _, exists := os.LookupEnv("USERNAME"); !exists {
		log.Fatalln("USERNAME env variable not declared")
	}

	if _, exists := os.LookupEnv("REMOTE_URL"); !exists {
		log.Fatalln("SERVER_URL env variable not declared")
	}

	if _, exists := os.LookupEnv("REMOTE_PORT"); !exists {
		log.Fatalln("PORT env variable not declared")
	}

	if _, exists := os.LookupEnv("LOCAL_URL"); !exists {
		log.Fatalln("LOCAL_URL env variable not declared")
	}

	if _, exists := os.LookupEnv("LOCAL_PORT"); !exists {
		log.Fatalln("LOCAL_URL env variable not declared")
	}
}

func startServer(c *gumble.Client) {
	http.HandleFunc("/", indexHandler)
	http.Handle("/files", http.StripPrefix("/files", http.FileServer(http.Dir("./files"))))
	http.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		files, err := checkForm(r)
		if err != nil {
			log.Println(err)
			http.Error(w, fmt.Sprintln(err), http.StatusBadRequest)
			return
		}

		var filename string
		if len(files) == 1 {
			filename, err = saveSingleFile(files)
		} else {
			filename, err = compressAndSave(files)
		}

		if err != nil {
			log.Println(err)
			http.Error(w, "File save error", http.StatusBadRequest)
			return
		}

		c.Self.Channel.Send(fmt.Sprintf("A new file is <a href=\"%s:%s/files/%s\">HERE</a>", os.Getenv("LOCAL_URL"), os.Getenv("LOCAL_PORT"), filename), false)
		go deleteFile(filename)
		http.ServeFile(w, r, "./static/thank_you.html")
	})

	err := http.ListenAndServe(":"+os.Getenv("LOCAL_PORT"), nil)
	if err != nil {
		log.Fatalln(err)
	}
}

func deleteFile(filename string) {
	time.Sleep(30 * time.Minute)
	if err := os.Remove("./files/" + filename); err != nil {
		log.Println(err)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./static/index.html")
}

func checkForm(r *http.Request) ([]*multipart.FileHeader, error) {

	if r.Method != "POST" {
		return nil, errors.New("invalid http method")
	}

	err := r.ParseMultipartForm(2 << 30)
	if err != nil {
		return nil, err
	}

	if len(r.MultipartForm.File) != 1 {
		return nil, errors.New("invalid form length")
	}

	files, exists := r.MultipartForm.File["files"]
	if !exists {
		return nil, errors.New("files does not exists")
	}

	return files, nil
}

func saveSingleFile(files []*multipart.FileHeader) (string, error) {
	file, err := files[0].Open()
	if err != nil {
		return "", err
	}

	data, err := io.ReadAll(file)
	if err != nil {
		return "", err
	}

	dirName := uuid.NewString()
	os.Mkdir("./files/"+dirName, 0777)
	filepath := fmt.Sprintf("%s/%s", dirName, url.QueryEscape(files[0].Filename))
	err = os.WriteFile(fmt.Sprintf("./files/%s", filepath), data, 0666)
	if err != nil {
		return "", err
	}

	return filepath, nil
}

func compressAndSave(files []*multipart.FileHeader) (string, error) {
	filename := uuid.NewString() + ".zip"
	outputFile, err := os.Create(fmt.Sprintf("./files/%s", filename))
	if err != nil {
		return "", err
	}

	outWriter := zip.NewWriter(outputFile)
	for _, f := range files {

		file, err := f.Open()
		if err != nil {
			return "", err
		}

		fileWriter, err := outWriter.Create(f.Filename)
		if err != nil {
			return "", err
		}

		data, err := io.ReadAll(file)
		if err != nil {
			return "", err
		}

		_, err = fileWriter.Write(data)
		if err != nil {
			return "", err
		}
	}

	err = outWriter.Close()
	if err != nil {
		return "", err
	}

	return filename, nil
}
