module gitlab.com/mumble-file-bot

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/joho/godotenv v1.3.0
	layeh.com/gumble v0.0.0-20200818122324-146f9205029b
)
